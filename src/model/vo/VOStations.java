package model.vo;

import java.util.Date;

public class VOStations 
{
  private int id;
  
  private String name;
  
  private String cityName;
  
  private int dpcapacity;
  
  
  public VOStations(int ID,String nombre, String nombreCiudad, int dpc)
  {
	  id = ID;
	  
	  name = nombre;
	  
	  cityName = nombreCiudad;
	  
	  dpcapacity= dpc;
	  
  }
  
  public int getId()
  {
	  return id;
  }
  
  public String getStationName()
  {
	  return name;
	  
  }
  
  public String getCityName()
  {
	  return cityName;
  }
  
  
  public int getDPCacity()
  {
	  return dpcapacity;
  }
  
}
