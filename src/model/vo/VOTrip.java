package model.vo;

import java.sql.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	
	private int bikeId;
	
	private int duration;
	
	private int fromStationId;
	
	private String fromStationName;
	
	private int toStationId;
	
	private String toStationName;
	
	private String gender;
	
	@SuppressWarnings("unused")
	private int birthYear;
	
	
	public VOTrip(int iD,  int bikeID, int duratioN, int fromId, String fromName, int toId, String toName, String gendeR)
	{
		id = iD;
		
		bikeId = bikeID;
		
		duration = duratioN;
		
		fromStationId = fromId;
		
		fromStationName = fromName;
		
		toStationId = toId;
		
		toStationName = toName;
		
		gender = gendeR;

		
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		
		return id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return duration;
	}

	public int getBikeId()
	{
		return bikeId;
	}
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		
		return fromStationName;
	}
	public int getFromStationId()
	{
		return fromStationId;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStationName;
	}
	
	public int getToStationId()
	{
		return toStationId;
	}
	
	public String getGender()
	{
		return gender;
	}
}
