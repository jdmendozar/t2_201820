package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {
	

	Integer getSize();
	
	T getFirst();
	
	void addObject(T t);
	
	void addAtK(int pos, T e);
	
	T getElement (int pos);
	
	void remove(T t) throws Exception;
	
	void removeAtK(int pos) throws Exception;

}
