package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStations;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoubleLinkedList<VOTrip> trips;

	private DoubleLinkedList<VOStations> stations;

	public void loadStations (String stationsFile)
	{
		stations = new DoubleLinkedList<VOStations>();
		File file = new File(stationsFile);

		
		try {
			CSVReader reader = new CSVReader (new FileReader(file));
			reader.readNext();
			String[] nextline;
			while((nextline=reader.readNext()) != null)
			{
				if(nextline != null)
				{

					int id = Integer.parseInt(nextline[0]);

					String nombre = nextline[1];

					String nombreCiudad = nextline[2];

					int dpc = Integer.parseInt(nextline[5]);

					VOStations nuevo = new VOStations(id, nombre, nombreCiudad, dpc);

					stations.addAtK(0, nuevo);

					nextline = null;
				}

			}

			reader.close();

		} catch ( IOException e ) 
		{

			e.printStackTrace();
		}
	}


	public void loadTrips (String tripsFile) {

		trips = new DoubleLinkedList<VOTrip>();
		 
		File file = new File(tripsFile);
		
		try
		{
			CSVReader reader = new CSVReader (new FileReader(file));
			reader.readNext();
			String[] nextline;
			while((nextline=reader.readNext()) != null)
			{
				if(nextline != null)
				{
					int id = Integer.parseInt(nextline[0]);

					int bikeID = Integer.parseInt(nextline[3]);
					
					int duratioN = Integer.parseInt(nextline[4]);
					
					int fromId = Integer.parseInt(nextline[5]);
					
					String fromName = nextline[6];
					
					int toId = Integer.parseInt(nextline[7]);
					
					String toName = nextline[8];
					
					String gendeR = null;
					if(nextline.length>10)
					{
					 gendeR = nextline[10];
					}
					VOTrip nuevo = new VOTrip(id, bikeID, duratioN, fromId, fromName, toId, toName, gendeR);

					trips.addAtK(0, nuevo);
				}
			}
			reader.close();
			
		}
		catch ( IOException e ) 
		{

			e.printStackTrace();
		}

	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) 
	{

		DoubleLinkedList <VOTrip> resp;
		resp = new DoubleLinkedList<VOTrip>();

		Iterator<VOTrip> gTrips = trips.iterator();

		while(gTrips.hasNext())
		{
			VOTrip actual = gTrips.next();

			if(actual.getGender()!= null)
			{
				if(actual.getGender().toLowerCase().equals(gender.toLowerCase()))
				{
					resp.addAtK(0, actual);
				}
			}
		}
		return resp;


	}

	@Override 
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) 
	{
		DoubleLinkedList<VOTrip> resp;

		resp = new DoubleLinkedList<VOTrip>();

		Iterator<VOTrip> gTrips = trips.iterator();

		while(gTrips.hasNext())
		{
			VOTrip actual = gTrips.next();

			if(actual.getToStationId()==stationID)
			{
				resp.addAtK(0, actual);
			}
		}
		return (DoublyLinkedList<VOTrip>) resp;
	}	


}
