package Test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;

public class datasTest {

	private DoubleLinkedList<Integer> lista;
	@Test
	public void test() 
	{
		 lista = new DoubleLinkedList<Integer>();
		 
		 try{
			 lista.addObject(1);
			 assertTrue(lista.getFirst()==1);
			 assertTrue(lista.getSize()==1);
			 System.out.println("1");
			 
			 lista.addObject(4);
			 lista.addObject(7);
			 lista.addObject(3);
			 System.out.println("2");
			 
			 assertTrue(lista.getSize() == 4);
			 assertTrue(lista.getElement(3)==3);
			 assertTrue(lista.getElement(2)==7);
			 System.out.println("3");
			 
			 lista.addAtK(2, 5);
			 assertTrue(lista.getSize()==5);
			 assertTrue(lista.getElement(2)==5);
			 System.out.println("4");
			 
			 lista.addAtK(2, 2);
			 assertTrue(lista.getSize()==6);
			 assertTrue(lista.getElement(2)==2);
			 System.out.println("5");
			 
			 lista.remove(2);
		     System.out.println("6");
			 assertTrue(lista.getSize()==5);
			 assertTrue(lista.getElement(3)==5);
			 
			 lista.removeAtK(0);
			 System.out.println("7");
			 assertTrue(lista.getSize()==4);
			 assertTrue(lista.getElement(0)==4);
			 
			 
		 }catch (Exception e) {
			fail ("Lanza excepcion esta madre -_-."+e);
		}
	}
	
	@Test
	public void testIterator()
	{
		DoubleLinkedList<Integer> prueba = new DoubleLinkedList<>();
		prueba.addObject(1);
		Iterator<Integer>iter = prueba.iterator();
		assertTrue(iter.hasNext());
		
		try {
			prueba.removeAtK(0);
		} catch (Exception e) {
			
		}
		
		for (int i = 0; i < 10; i++) 
		{
		prueba.addObject(i);
		}
		iter = prueba.iterator();
		int p =0;
		while(iter.hasNext())
		{
			int i = iter.next();
			assertTrue("el valor "+p +"no es igual al valor"+i, p ==i);
			p++;
		}
	}

}
