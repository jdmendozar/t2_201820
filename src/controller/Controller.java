package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;
import view.DivvyTripsManagerView;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	

	
	public static void loadStations() 
	{
		manager.loadStations("./Data/Divvy_Stations_2017_Q3Q4.csv");
	}
	
	public static void loadTrips()
	{
		manager.loadTrips("./Data/Divvy_Trips_2017_Q4.csv");
	}
		
	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}
	
	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
